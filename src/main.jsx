// 'use strict'
import React from 'react';
import ReactDOM from 'react-dom';

import App from './app/containers/App';

ReactDOM.render(<App />, document.getElementById('root')); //eslint-disable-line