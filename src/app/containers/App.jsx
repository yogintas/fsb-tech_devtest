import React, { Component } from 'react';
import xhr from 'axios';
import messageBroker from '../services/messageBroker';
import Component1 from '../components/Component1';
import Component2 from '../components/Component2';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      total: null,
      players: null
    };
  }

  componentDidMount() {
    xhr.get('/data/sample.json').then(
      ({ data: { players } }) => this.setState({ players })
    );
  }

  toggleSelection(selection) {
    messageBroker.publish('toggleSelection', selection);
  }

  render() {
    return (
      <div className="app container">
        <Component1 />
        <Component2
          players={this.state.players}
          clickHandler={this.toggleSelection}
        />
      </div>
    );
  }
}
