import React, { Component } from 'react';

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: false
    };
    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler(player) {
    this.props.clickHandler(player);
    this.setState({
      selected: !this.state.selected
    });
  }

  render() {
    const { player, price } = this.props;
    return (
      <div className="item">
        <span
          style={{ textDecoration: this.state.selected ? 'underline' : 'none' }}
          className="player">
          { player }
        </span>
        <button
          className="price"
          onClick={() => this.clickHandler(player)}>
          { price }
        </button>
      </div>
    );
  }
}

export default Item;
