/* eslint-disable */
import React from 'react';
import renderer from 'react-test-renderer';
import Component1 from './Component1';

describe('Generic app', () => {
  it('renders correctly', () => {
    const comp1 = renderer.create(
      <Component1 />).toJSON();
    expect(comp1).toMatchSnapshot();
  });
});
