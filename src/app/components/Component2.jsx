import React from 'react';
import uid from 'uid';
import Item from './Item';

const Component2 = ({ players, clickHandler }) => (
  <div className="component2">
    {
      players && players.map(player => (
        <Item
          key={uid()}
          {...player}
          clickHandler={clickHandler}
        />
      ))
    }
  </div>
);

export default Component2;
