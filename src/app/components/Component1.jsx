import React, { Component } from 'react';
import messageBroker from '../services/messageBroker';

const { subscribe } = messageBroker;

export default class Component1 extends Component {
  constructor() {
    super();
    this.state = {
      selections: [],
    };
    this.listener = this.listener.bind(this);
  }

  componentWillMount() {
    subscribe('toggleSelection', this.listener);
  }

  listener(selection) {
    const { selections } = this.state;
    const newSelections = [].concat(selections);
    const indexOfSelection = newSelections.indexOf(selection);
    if (indexOfSelection === -1) {
      newSelections.push(selection);
    } else {
      newSelections.splice(indexOfSelection, 1);
    }
    this.setState({ selections: newSelections });
  }

  render() {
    return (
      <div className="component1">
        { this.state.selections.length }
      </div>
    );
  }
}

