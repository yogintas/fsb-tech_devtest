/* eslint-disable */
import React from 'react';
import sinon from 'sinon';
import { shallow, mount} from 'enzyme';
import renderer from 'react-test-renderer';
import Component2 from './Component2';
import sampleData from '../data/sample.json'

const { players } = sampleData;

describe('Generic component2', () => {
  it('renders correctly', () => {
    const comp2 = renderer.create(
      <Component2 players={players} />).toJSON();
    expect(comp2).toMatchSnapshot();
  });

  it('calls clickHandler with player name', () => {
    const mockFn = sinon.spy();

    const comp2 = mount(
      <Component2 players={players} clickHandler={mockFn} />
    );

    console.log(comp2.html());

    const priceButton = comp2.find('.price').at(0);
    priceButton.simulate('click');

    console.log(mockFn.args[0][0])

    expect(mockFn.calledOnce).toBeTruthy();
    expect(mockFn.args[0][0]).toEqual('Royal');
  });
});
