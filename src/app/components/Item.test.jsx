/* eslint-disable */
import React from 'react';
import renderer from 'react-test-renderer';
import Item from './Item';

describe('Generic app', () => {
  it('renders correctly', () => {
    const item = renderer.create(
      <Item />).toJSON();
    expect(item).toMatchSnapshot();
  });
});
