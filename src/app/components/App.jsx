import React, { Component } from 'react';
import Component1 from './Component1';
import Component2 from './Component2';

export default class App extends Component {
  render() {
    return (
      <div className="app container">
        <Component1 />
        <Component2 />
      </div>
    );
  }
}
