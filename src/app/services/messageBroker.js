class MessageBroker {
  constructor() {
    this.topics = {};
    this.subscribe = this.subscribe.bind(this);
  }

  subscribe(topic, listener) {
    if (!this.topics[topic]) this.topics[topic] = [];
    this.topics[topic].push(listener);
  }

  publish(topic, data) {
    if (!this.topics[topic] || this.topics[topic].length < 1) return;
    this.topics[topic].forEach(
      listener => listener(data || {})
    );
  }
}

const singleton = new MessageBroker();

export default singleton;
