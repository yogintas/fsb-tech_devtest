module.exports = [
  {
    test: /\.jsx?$/,
    exclude: /(node_modules)/,
    loader: 'babel-loader',
    query: {
      presets: ['react', 'es2015', 'stage-2'],
    }
  }
];
